import React, {Component} from 'react';
import {StyleSheet, StatusBar, View, Text} from 'react-native';
import AppNavigator from './src/managers/AppNavigator';
import Icons from './src/components/Icons';

class SplashScreen extends React.PureComponent {
  render() {
    const viewStyles = [styles.container, {backgroundColor: '#ffffff'}];
    const textStyles = {
      color: 'black',
      fontSize: 24,
      fontWeight: 'bold',
    };

    return (
      <View style={viewStyles}>
        <StatusBar hidden />
        <Icons name={'logo'} />
        <Text style={textStyles}>لمون</Text>
      </View>
    );
  }
}

export default class Myapp extends Component {
  constructor() {
    super();
    this.state = {
      isLoading: true,
    };
  }
  Hide_Splash_Screen = () => {
    this.setState({
      isLoading: false,
    });
  };

  componentDidMount() {
    var that = this;
    setTimeout(function () {
      that.Hide_Splash_Screen();
    }, 3000);
  }

  render() {
    if (this.state.isLoading) {
      return <SplashScreen />;
    }
    return <AppNavigator />;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
