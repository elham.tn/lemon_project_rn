import React, {
  Component
} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet
} from 'react-native';
import COLOR from '../styles/Color';
import Btn from 'react-native-micro-animated-button';

class ButtomBasic extends Component {

  constructor(props) {
    super(props);
  }

 onShowError() {
	 console.log('Error');
	 this.Btn.error();
	}

  render() {
    const {
      text,
      onPress,
			onError
    } = this.props;

    return ( <
      Btn
			style = {
        styles.buttonStyle
      }
      labelStyle = {
        styles.textStyle
      }
      label = {
        text
      }
      onPress = {
        () => onPress()
      }
			onError={() => onShowError()}
      noFill = 'true'
      noBorder = 'true'
      successIcon = "check"
      foregroundColor = '#FFFFFF'
      labelColor = '#FFFFFF'
      expandOnFinish = 'true' />
    );
  }
}

ButtomBasic.propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
	onError: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
  textStyle: {
    textAlign: 'center',
    color: COLOR.WHITE,
    fontSize: 16,
  },

  buttonStyle: {
    backgroundColor: COLOR.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    height: 40,
    width: 300,
    fontSize: 16,
    borderRadius: 5,
  }

});

export default ButtomBasic;
