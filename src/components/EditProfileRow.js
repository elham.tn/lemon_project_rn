/* eslint-disable prettier/prettier */
import React from 'react';
import { StyleSheet, View, Image, Text } from 'react-native';

function EditProfileRow(props) {
  return (
    <View style={[styles.container]}>
      <View style={styles.insideContainer}>
        <Image
          source={props.icon}
          style={styles.imageStyle}
        />
        <Text style={styles.headerStyle}>{props.title}</Text>
        <Text style={styles.contentStyle}>{props.value}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '95%',
    height: 60,
    marginTop: 12,
    flexDirection: 'row',
  },

  imageStyle: {
    right: 0,
    width: 48,
    height: 48,
    position: 'absolute',
    margin: 5,
  },

  headerStyle: {
    top: 10,
    right: 70,
    color: '#121212',
    position: 'absolute',
    fontSize: 16,
    fontWeight:'bold',
    fontFamily: 'iransansdium',
  },

  contentStyle: {
    top: 30,
    right: 70,
    color: '#121212',
    position: 'absolute',
    fontSize: 14,
    fontFamily: 'iransanslight',
  },

  insideContainer: {
    width: 200,
    height: 48,
    position: 'absolute',
    right: 0,
  },
});

export default EditProfileRow;
