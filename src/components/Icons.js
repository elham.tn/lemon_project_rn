/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import { Image } from 'react-native';
import COLOR from '../styles/Color';

const Icon = ({ name }) => {
  if (name === 'login') {
    return (
      <Image
        source={require('../images/login.png')}
        style={{
          width: 300,
          height: 300,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 50,
          marginBottom: 8,
        }}
      />
    );
  } else if (name === 'logo') {
    return (
      <Image
        source={require('../images/logo.png')}
        style={{
          width: 300,
          height: 300,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      />
    );
  } else if (name === 'edit_lock') {
    return (
      <Image
        source={require('../images/padlock.png')}
        style={{
          width: 20,
          height: 20,
          margin: 4,
          marginLeft: 12,
          tintColor: COLOR.PRIMARY,
        }}
      />
    );
  } else if (name === 'password') {
    return (
      <Image
        source={require('../images/login.png')}
        style={{
          width: 180,
          height: 180,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 100,
          marginBottom: 16,
        }}
      />
    );
  } else if (name === 'code') {
    return (
      <Image
        source={require('../images/confirmation.png')}
        style={{
          width: 300,
          height: 300,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 50,
          marginBottom: 8,
        }}
      />
    );
  } else if (name === 'edit_mobile') {
    return (
      <Image
        source={require('../images/smartphone.png')}
        style={{
          width: 20,
          height: 20,
          margin: 4,
          tintColor: COLOR.PRIMARY,
        }}
      />
    );
  } else if (name === 'profile') {
    return (
      <Image
        source={require('../images/happy_people.png')}
        style={{
          width: '90%',
          height: 250,
          marginTop: 16,
        }}
      />
    );
  }
  else if (name === 'settings') {
    return (
      <Image
        source={require('../images/settings.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  } else if (name === 'list') {
    return (
      <Image
        source={require('../images/list.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  } else if (name === 'mb') {
    return (
      <Image
        source={require('../images/message_box.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  }
  else if (name === 'back') {
    return (
      <Image
        source={require('../images/back.png')}
        style={{
          width: 28,
          height: 28,
          margin: 16,
        }}
      />
    );
  }
  else if (name === 'back-small') {
    return (
      <Image
        source={require('../images/back.png')}
        style={{
          width: 16,
          height: 16,
          margin: 4,
        }}
      />
    );
  }
  else if (name === 'wallet') {
    return (
      <Image
        source={require('../images/wallet.png')}
        style={{
          width: 90,
          height: 90,
          rotation: -15,
          marginTop: 20,
          marginLeft: 10,
          opacity: 0.5,
        }}
      />
    );
  }
  else if (name === 'copy') {
    return (
      <Image
        source={require('../images/copy.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  } else if (name === 'share') {
    return (
      <Image
        source={require('../images/share.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  } else if (name === 'info') {
    return (
      <Image
        source={require('../images/information.png')}
        style={{
          width: 40,
          height: 40,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 16,
        }}
      />
    );
  } else if (name === 'calendar') {
    return (
      <Image
        source={require('../images/calendar.png')}
        style={{
          width: 35,
          height: 35,
          alignItems: 'center',
          justifyContent: 'center',
          marginTop: 8,
        }}
      />
    );
  } else if (name === 'star') {
    return (
      <Image
        source={require('../images/star.png')}
        style={{
          width: 16,
          height: 16,
          marginLeft:5,
        }}
      />
    );
  }
};

export default Icon;
