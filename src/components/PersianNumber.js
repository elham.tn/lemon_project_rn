/* eslint-disable prettier/prettier */
/* eslint-disable no-trailing-spaces */
import React, { Component } from 'react';
import { Text } from 'native-base';

class PersianNumber extends Component {
  render() {
    let size = this.props.size;
    let color = this.props.color;
    let top_margin = this.props.t_margin;
    let bottom_margin = this.props.b_margin;
    let en_number = this.props.number.toString();
    let persianDigits = '۰۱۲۳۴۵۶۷۸۹';
    let persianMap = persianDigits.split('');
    let persian_number = en_number.replace(/\d/g, function (m) {
      // eslint-disable-next-line radix
      return persianMap[parseInt(m)];
    });

    // eslint-disable-next-line react-native/no-inline-styles
    return <Text style={{
      fontSize: size, 
      color: color, 
      marginTop: top_margin,
      marginBottom: bottom_margin,
      textAlign: 'center',
    }}>{persian_number}</Text>;
  }
}

export default PersianNumber;
