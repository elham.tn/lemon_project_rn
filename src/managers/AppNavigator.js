/* eslint-disable prettier/prettier */
import ScrLogin from '../screens/ScrLogin.js';
import ScrCodeConfirmation from '../screens/ScrCodeConfirmation.js';
import ScrProfile from '../screens/ScrProfile.js';
import ScrProviders from '../screens/ScrProviders.js';
import ScrProvidersProfile from '../screens/ScrProvidersProfile.js';
import ScrHistory from '../screens/ScrHistory.js';
import ScrSettings from '../screens/ScrSettings.js';
import ScrEditProfile from '../screens/ScrEditProfile.js';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
const Stack = createStackNavigator();

function RootStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="ScrLogin"
        screenOptions={{ gestureEnabled: false }}>

        <Stack.Screen
          name="ScrLogin"
          component={ScrLogin}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrCodeConfirmation"
          component={ScrCodeConfirmation}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrProfile"
          component={ScrProfile}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrProviders"
          component={ScrProviders}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrProvidersProfile"
          component={ScrProvidersProfile}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrHistory"
          component={ScrHistory}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrSettings"
          component={ScrSettings}
          options={{ headerShown: false }}
        />

        <Stack.Screen
          name="ScrEditProfile"
          component={ScrEditProfile}
          options={{ headerShown: false }}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default RootStack;
