/* eslint-disable prettier/prettier */
import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  Alert,
  AsyncStorage,
} from 'react-native';
import { Text } from 'native-base';
import COLOR from '../styles/Color';
import Icons from '../components/Icons';
import Btn from 'react-native-micro-animated-button';
import { BackHandler } from 'react-native';

console.disableYellowBox = true;
export default class ScrCodeConfirmation extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      verify_code: '',
      correct: true,
      user_id: 0,
      user_token: '',
      user_active_complex: 0,
      user_mobile: '',
      user_phone: '',
      user_address: '',
      user_first_name: '',
      user_last_name: '',
      user_email: '',
      user_coin: 0,
      valueToshow: '',
      user_code_melli: '',
    };

    this._retrieveData();

    this.onBackPress = this.onBackPress.bind(this);
    this.onPressLogin = this.onPressLogin.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  async onFetchConfirm() {
    try {
      fetch('https://mylemon.ir/api/v1/login/verify', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          verify_code: this.state.verify_code,
          user_id: this.state.user_id,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 1) {
            if (responseJson.code == 0) {
              console.log(responseJson.content.user.token);

              this.setState({
                user_id: responseJson.content.user.id + '',
                user_token: responseJson.content.user.token,
                user_first_name: responseJson.content.user.fname,
                user_last_name: responseJson.content.user.lname,
                user_email: responseJson.content.user.email,
                user_address: responseJson.content.user.address,
                user_code_melli: responseJson.content.user.ncode,
                user_phone: responseJson.content.user.phone,
                user_mobile: responseJson.content.user.mobile,
                user_active_complex: responseJson.content.complex_active + '',
              });

              this._storeData();
            } else if (responseJson.code == 2002) {
              this.btn.error();
              Alert.alert(
                ' خطا ',
                responseJson.message,
                [{ text: 'باشه', onPress: () => console.log('NO Pressed') }],
                { cancelable: false },
              );
            }
          }
        })
        .catch(error => {
          console.error(error);
        });
    } catch (errors) {
      alert(errors);
    }
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrLogin');
    return true;
  };

  onPressLogin() {
    if (this.state.verify_code == '') {
      alert('لطفا کد را وارد کنید');
      this.btn.error();
    } else if (this.state.verify_code < 5) {
      this.btn.error();
    } else {
      this.onFetchConfirm();
    }
  }

  onChangeText(value) {
    this.setState({
      verify_code: value,
    });
    if (this.state.verify_code.length == 5) {
      Keyboard.dismiss();
    }
    this.btn.reset();
  }

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('@KEY_FIRST_TIME', 'false');
      await AsyncStorage.setItem('@KEY_SIGN_IN', 'true');
      await AsyncStorage.setItem('@KEY_USER_ID', this.state.user_id);
      await AsyncStorage.setItem('@KEY_USER_TOKEN', this.state.user_token);
      await AsyncStorage.setItem('@KEY_USER_PHONE', this.state.user_phone);
      await AsyncStorage.setItem('@KEY_USER_ADDRESS', this.state.user_address);
      await AsyncStorage.setItem('@KEY_USER_EMAIL', this.state.user_email);
      await AsyncStorage.setItem('@KEY_USER_FIRST_NAME', this.state.user_first_name);
      await AsyncStorage.setItem('@KEY_USER_LAST_NAME', this.state.user_last_name);
      await AsyncStorage.setItem('@KEY_USER_MOBILE', this.state.user_mobile);
      await AsyncStorage.setItem('@KEY_USER_CODE_MELLI', this.state.user_code_melli);
      await AsyncStorage.setItem('@KEY_USER_ACTIVE_COMPLEX', this.state.user_active_complex);

      this.props.navigation.navigate('ScrProfile');
    } catch (error) {
      // Error retrieving data
    }
  };

  _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('@KEY_USER_ID');
      if (value !== null) {
        // Our data is fetched successfully

        this.setState({ user_id: value, });
        console.log(this.state.user_id + '   successfully');
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  render() {
    const isCorrect = this.state.correct;
    const { navigate } = this.props.navigation;

    return (
      <ScrollView
        style={{
          flex: 1,
          backgroundColor: COLOR.COLOR_BACKGROUND,
        }}>
        <KeyboardAvoidingView
          style={{ flex: 1, }}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <View style={styles.containerStyle}>
            <StatusBar backgroundColor="white" barStyle="dark-content" />

            <Icons name={'code'} />

            <Text style={styles.titleStyle}>تایید کد </Text>

            <Text style={styles.contentStyle}>
              {' '}
              لطفا کد ارسال شده را در کادر زیر وارد کنید.
            </Text>

            <View style={styles.inputBox}>
              <Icons name={'edit_lock'} />

              <TextInput
                style={styles.input}
                textAlign="center"
                placeholder="******"
                keyboardType="numeric"
                maxLength={6}
                onChangeText={(verify_code) => this.onChangeText(verify_code)}
              />
            </View>

            <Btn
              style={styles.StyleButton}
              labelStyle={styles.defaultLabelStyle}
              label=" بررسی"
              onPress={() => this.onPressLogin()}
              ref={(ref) => (this.btn = ref)}
              noFill="true"
              noBorder="true"
              successIcon="check"
              foregroundColor="#FFFFFF"
              labelColor="#FFFFFF"
              expandOnFinish="true"
              errorLabel="خطا"
              errorForegroundColor="#db4437"
            />

            <Text
              onPress={() =>
                this.props.navigation.navigate('SrcCodeConfirmation')
              }
              style={styles.txtLinkStyle}>
              {' '}
              کدی دریافت نکردید؟ ارسال مجدد
            </Text>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: COLOR.COLOR_BACKGROUND,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  StyleButton: {
    backgroundColor: COLOR.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    height: 40,
    width: 300,
    fontSize: 16,
    borderRadius: 5,
  },

  inputBox: {
    width: 300,
    height: 40,
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: COLOR.PRIMARY,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 8,
  },

  input: {
    flex: 1,
    width: 300,
    height: 40,
    paddingRight: 32,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#002f6c',
    fontSize: 16,
    alignItems: 'center',
  },

  titleStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
  },

  contentStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8,
    marginBottom: 32,
    color: COLOR.TEXT_COLOR,
  },

  defaultLabelStyle: {
    color: COLOR.WHITE,
    fontSize: 16,
  },

  txtLinkStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 32,
    color: COLOR.TEXT_COLOR,
  },
});
