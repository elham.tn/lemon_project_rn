/* eslint-disable prettier/prettier */
import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import EditProfileRow from '../components/EditProfileRow';
import { BackHandler } from 'react-native';
import { Toolbar } from 'react-native-material-ui';
import COLOR from '../styles/Color';

export default class ScrEditProfile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onBackPress = this.onBackPress.bind(this);
    this._retrieveData();

    this.state = {
      u_id: 0,
      u_token: '',
      u_fname: '',
      u_lname: '',
      u_mobile: '',
      u_phone: '',
      u_email: '',
      u_address: '',
      u_code: '',
    };

  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);

  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrSettings');
    return true;
  };

  _retrieveData = async () => {
    try {
      const user_id = await AsyncStorage.getItem('@KEY_USER_ID');
      const user_token =  await AsyncStorage.getItem('@KEY_USER_TOKEN');
      const user_phone =  await AsyncStorage.getItem('@KEY_USER_PHONE');
      const user_address =  await AsyncStorage.getItem('@KEY_USER_ADDRESS');
      const user_email =  await AsyncStorage.getItem('@KEY_USER_EMAIL');
      const user_f_name = await AsyncStorage.getItem('@KEY_USER_FIRST_NAME');
      const user_l_name = await AsyncStorage.getItem('@KEY_USER_LAST_NAME');
      const user_mobile =  await AsyncStorage.getItem('@KEY_USER_MOBILE');
      const user_code = await AsyncStorage.getItem('@KEY_USER_CODE_MELLI');

      if (user_id !== null) {
        this.setState({
          u_id: user_id,
          u_token: user_token,
          u_address:user_address,
          u_code:user_code,
          u_mobile:user_mobile,
          u_email:user_email,
          u_fname:user_f_name,
          u_lname:user_l_name,
          u_phone:user_phone,
        });
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  render() {
    return (
      <SafeAreaView>

        <Toolbar
          style={{
            container: { backgroundColor: 'white' },
            titleText: { color: '#000', fontFamily: 'iransans', alignSelf: 'flex-start' },
            leftElement: { color: '#000' },
            rightElement: { color: COLOR.PRIMARY },
          }}
          rightElement="done"
          leftElement="arrow-back"
          centerElement="ویرایش پروفایل"
          onLeftElementPress={(label) => { console.log(label); }}
        />

        <ScrollView>
          <View style={styles.container}>
            <StatusBar
              animated={true}
              barStyle="dark-content"
              backgroundColor="white"
            />

            <EditProfileRow title={'نام'} value={this.state.u_fname} icon={require('../images/name.png')} onPress={() => this.RBSheet.open()}/>
            <EditProfileRow title={'نام خانوادگی'} value={this.state.u_lname} icon={require('../images/name.png')} />
            <EditProfileRow title={'شماره موبایل'} value={this.state.u_mobile} icon={require('../images/smartphone.png')} />
            <EditProfileRow title={'تلفن ثابت'} value={this.state.u_phone} icon={require('../images/phone.png')} />
            <EditProfileRow
              title={'پست الکترونیکی'}
              value={this.state.u_email}
              icon={require('../images/mail.png')}
            />
            <EditProfileRow title={'آدرس'} value={this.state.u_address} icon={require('../images/adress.png')} />
            <EditProfileRow title={'کد ملی'} value={this.state.u_code} icon={require('../images/card.png')} />
          </View>
        </ScrollView>
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          height={300}
          duration={250}
          customStyles={{
            container: {
              justifyContent: 'center',
              alignItems: 'center',
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            },
          }}>
          <View style={styles.BottomAvatarContainer} />
        </RBSheet>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLOR.COLOR_BACKGROUND,
  },

  BottomAvatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'column',
  },

  RowAvatarContainer: {
    height: 56,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'row',
  },

  imgBackStyle: {
    width: 20,
    height: 20,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },

  imgBackContainerStyle: {
    position: 'absolute',
    left: 16,
    top: 16,
    padding: 8,
    flexDirection: 'row',
  },

  image: {
    width: 180,
    height: 180,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  avatarStyle: {
    width: 72,
    height: 72,
    borderRadius: 72 / 2,
    borderWidth: 2,
    borderColor: '#FFFFFF',
    marginHorizontal: 16,
  },

  button: {
    top: 170,
    left: 48,
    width: 107,
    height: 25,
    backgroundColor: 'rgba(230, 230, 230,1)',
    position: 'absolute',
    borderRadius: 5,
    elevation: 0,
  },


  imageStack: {
    width: 200,
    height: 200,
    marginTop: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },

});
