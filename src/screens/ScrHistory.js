/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Text,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  Alert,
  AsyncStorage,
  Image,
} from 'react-native';
import COLOR from '../styles/Color';
import { Toolbar } from 'react-native-material-ui';
import { BackHandler } from 'react-native';

export default class ScrHistory extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onEndReached = this.onEndReached.bind(this);
    this.renderFooter = this.renderFooter.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.onFetchHistory = this.onFetchHistory.bind(this);
    this.addToHistory = this.addToHistory.bind(this);

    this.state = {
      isRefreshing: false,
      waiting: false,
      history: [],
      historyBefore: [],
      historyNotSorted: [],
      loading: true,
      u_id: 0,
      u_token: '',
      icon: '',
      isFirstRow: false,
      isLastRow: false,
    };

  }

  _retrieveData = async () => {
    try {
      const user_id = await AsyncStorage.getItem('@KEY_USER_ID');
      const user_token = await AsyncStorage.getItem('@KEY_USER_TOKEN');
      const c_id = await AsyncStorage.getItem('@KEY_PROVIDER_ID');

      if (user_id !== null) {
        this.setState({
          u_id: user_id,
          u_token: user_token,
          c_id: c_id,
        });
        this.onFetchHistory();
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  async onFetchHistory() {
    try {
      this.setState({ loading: true });
      fetch('https://mylemon.ir/api/v1/complex/timeline', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: this.state.u_token,
          user_id: this.state.u_id,
          complex_id: this.state.c_id,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
         // console.log(responseJson);

          if (responseJson.status == 1) {
            if (responseJson.code == 0) {

              this.setState({
                historyNotSorted: [...this.state.historyNotSorted, ...responseJson.content.data],
                loading: false,
                isFetching: false,
              });
              this.addToHistory();

            } else if (responseJson.code == 2002) {

              Alert.alert(
                ' خطا ',
                responseJson.message,
                [{ text: 'باشه', onPress: () => console.log('NO Pressed') }],
                { cancelable: false },
              );
            }
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ error, loading: false, isFetching: false });
        });
    } catch (errors) {
      // eslint-disable-next-line no-alert
      alert(errors);
    }
  }

  componentDidMount() {
    this._retrieveData();
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrProvidersProfile');
    return true;
  };

  onRefresh() {
    //set initial data
  }

  onEndReached() {
    //fetch next data
  }

  renderFooter() {
    //show loading indicator
    if (this.state.waiting) {
      return <ActivityIndicator />;
    } else {
      return <Text />;
    }
  }

  addItem(date, title, content, icon) {
    const obj = { 'date': date, 'title': title, 'content': content, 'icon': icon };
    this.setState({
      historyBefore: [...this.state.historyBefore, obj],
    });
  }

  addToHistory() {
    for (let index = 0; index < this.state.historyNotSorted.length; index++) {
      const element = this.state.historyNotSorted[index];

      if (element.spend !== 0) {
        var spend = element.spend + '';
        spend = spend.substring(1);
        this.addItem(element.date, ' خرج شده', spend + ' اعتبار از حساب شما کسر گردید. ', require('../images/spending.png'));
      }
      if (element.gain !== 0) {
        this.addItem(element.date, '  دریافتی ', element.gain + ' اعتبار به حساب شما افزوده شد.  ', require('../images/gained.png'));

      }
      if (element.referred !== 0) {
        this.addItem(element.date, 'معرفی ', element.referred + ' اعتبار برای معرفی به حساب شما افزوده شد. ', require('../images/followers.png'));
      }
    }
    this.setState({
      history: this.state.historyBefore,
    });
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.rowStyle}>

        <View style={{
          width: 10,
          flexDirection: 'column',
          marginLeft:8,
        }}>

          <View style={{
            flex: 1,
            width: 4,
          }} >

            {
              (index == 0) ? null :
                (
                  <View>
                    <View style={styles.dot} />
                    <View style={styles.dot} />
                    <View style={styles.dot} />
                  </View>
                )
            }

          </View>

          <View style={{
            flex: 1,
            width: 4,
          }} />

          {
            (index == this.state.history.length - 1) ? null :
              (
                <View>
                  <View style={styles.dot} />
                  <View style={styles.dot} />
                  <View style={styles.dot} />
                </View>
              )
          }

        </View>

        <Image
          source={item.icon}
          style={styles.imageStyle}
        />
        <View style={styles.textRowStyle}>
          <Text style={styles.headerStyle}>{item.title}</Text>
          <Text style={styles.contentStyle}>{item.content}</Text>
        </View>
        <Text style={styles.contentStyle}>{item.date}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <Toolbar
          style={{
            container: { backgroundColor: 'white' },
            titleText: { color: '#000', fontFamily: 'iransans', alignSelf: 'flex-start' },
            leftElement: { color: '#000' },
          }}
          leftElement="arrow-back"
          centerElement="تاریخچه"
          onLeftElementPress={(label) => { console.log(label); }}
        />

        <View style={styles.MainContainer}>
          <StatusBar
            animated={true}
            barStyle="dark-content"
            backgroundColor="white"
          />

          {
            (this.state.loading)
              ?
              (<ActivityIndicator size="large" />)
              :
              (
                <FlatList
                  extraData={this.state}
                  padding={10}
                  data={this.state.history}
                  keyExtractor={(item, index) => index}
                  renderItem={this.renderItem}
                  removeClippedSubviews={true}
                  initialNumToRender={5}
                  minToRenderPerBatch={10}
                  maxToRenderPerBatch={100}
                  windowSize={7}
                  onRefresh={() => this.onRefresh()}
                  refreshing={this.state.isFetching}
                />
              )
          }

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: COLOR.COLOR_BACKGROUND,
  },

  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    marginLeft: 8,
    marginRight: 8,
    marginBottom: 5,
    marginTop: 8,
  },

  list: {
    flex: 1,
    marginLeft: 16,
    marginRight: 8,
    backgroundColor: COLOR.COLOR_BACKGROUND,
  },

  rowStyle: {
    flex: 1,
    flexDirection: 'row-reverse',
  },

  textRowStyle: {
    justifyContent: 'space-around',
    padding: 8,
  },

  imageStyle: {
    width: 60,
    height: 60,
    margin: 8,
  },

  headerStyle: {
    color: '#121212',
    fontSize: 18,
    flex: 1,
    fontWeight: 'bold',
    fontFamily: 'roboto-regular',
  },

  contentStyle: {
    color: '#121212',
    flex: 1,
    fontSize: 15,
    fontFamily: 'roboto-700',
    marginTop:5,
  },

  dot: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginTop: 4,
    marginBottom:4,
    backgroundColor: COLOR.PRIMARY_SECONDARY,
  },

});
