/* eslint-disable prettier/prettier */
import React from 'react';
import {
  Alert,
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  NetInfo,
  Platform,
  Keyboard,
} from 'react-native';
import { Text } from 'native-base';
import COLOR from '../styles/Color';
import Icons from '../components/Icons';
import Btn from 'react-native-micro-animated-button';
import { BackHandler } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';


console.disableYellowBox = true;

export default class ScrLogin extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      TxtUsername: '',
      correct: true,
      valueToshow: '',
      loading: true,
      dataSource: [],
    };

    this.onBackPress = this.onBackPress.bind(this);
    this.onPressLogin = this.onPressLogin.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }

  setDataToPassToConfirm(user_id) {
    console.log(this.state.valueToshow + " --------- " + user_id);
    this.setState({ id: user_id }, () => {
      AsyncStorage.setItem('@KEY_USER_ID', user_id);
    });
    console.log(AsyncStorage.getItem('@KEY_USER_ID') + " EL");
    this.props.navigation.navigate('SrcCodeConfirmation');
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === 'android') {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          Alert.alert('You are online!');
        } else {
          Alert.alert('You are offline!');
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange,
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange,
    );

    if (isConnected === false) {
      Alert.alert('You are offline!');
    } else {
      Alert.alert('You are online!');
    }
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  async onFetchLoginRecords() {
    try {
      fetch('https://mylemon.ir/api/v1/login', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          mobile: this.state.TxtUsername,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 1) {
            if (responseJson.code == 0) {
              console.log(responseJson.content);

              this.setState({
                valueToshow: responseJson.content.user_id + '',
              });

              this._storeData();
            } else if (responseJson.code === 2002) {
              this.btn.error();
              Alert.alert(
                ' خطا ',
                responseJson.message,
                [{ text: 'باشه', onPress: () => console.log('NO Pressed') }],
                { cancelable: false },
              );
            }
          }
        })
        .catch(error => {
          console.error(error);
        });
    } catch (errors) {
      alert(errors);
    }
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    Alert.alert(
      ' خروج از برنامه ',
      ' آیا می خواهید از برنامه خارج شوید؟',
      [
        { text: 'بله', onPress: () => BackHandler.exitApp() },
        { text: 'خیر', onPress: () => console.log('NO Pressed') },
      ],
      { cancelable: false },
    );
    return true;
  };

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('@KEY_USER_ID', this.state.valueToshow);
      this.props.navigation.navigate('ScrCodeConfirmation');
    } catch (error) {
      // Error saving data
    }
  }

  onPressLogin() {
    console.log(this.state.TxtUsername.length);
    if (this.state.TxtUsername.length == 0) {
      this.btn.error();
    } else if (this.state.TxtUsername.length < 11) {
      this.btn.error();
    } else if (!this.state.TxtUsername.startsWith('09')) {
      //alert('Invalid Number');
      this.btn.error();
    } else {
      this.onFetchLoginRecords();
    }
  }

  onChangeText(value) {
    this.setState({
      TxtUsername: value,
    });
    if (this.state.TxtUsername.length == 10) {
      Keyboard.dismiss();
    }
    this.btn.reset();
  }

  render() {
    // if (this.state.valueToshow == 'false') {
    //   this.props.navigation.navigate('ScrProfile');
    // }

    return (
      // eslint-disable-next-line react-native/no-inline-styles
      <ScrollView style={{
        flex: 1,
        backgroundColor: COLOR.COLOR_BACKGROUND,
      }}>
        <KeyboardAvoidingView
          style={{
            flex: 1,
          }}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <View style={styles.containerStyle}>
            <StatusBar backgroundColor="white" barStyle="dark-content" />

            <Icons name={'login'} />

            <Text style={styles.titleStyle}> خوش آمدید! </Text>

            <Text style={styles.contentStyle}>
              {' '}
              برای ورود و استفاده از امکانات لمون ، {'\n'}
              شماره موبایل خود را در کادر زیر وارد کنید.{' '}
            </Text>

            <View style={styles.inputBox}>
              <Icons name={'edit_mobile'} />

              <TextInput
                style={styles.input}
                textAlign="center"
                placeholder="۰۹xxxxxxxxx"
                keyboardType="numeric"
                maxLength={11}
                onChangeText={TxtUsername => this.onChangeText(TxtUsername)}
              />
            </View>

            <Btn
              style={styles.StyleButton}
              labelStyle={styles.defaultLabelStyle}
              label=" ورود "
              onPress={this.onPressLogin}
              ref={ref => (this.btn = ref)}
              noFill="true"
              noBorder="true"
              successIcon="check"
              foregroundColor="#FFFFFF"
              labelColor="#FFFFFF"
              expandOnFinish="true"
              errorLabel="خطا"
              errorForegroundColor="#db4437"
            />

            <Text
              style={{ marginTop: 50 }}
              onPress={() => {
                this.props.navigation.navigate('ScrProfile');
              }}>
              Profile
            </Text>
          </View>
        </KeyboardAvoidingView>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: COLOR.COLOR_BACKGROUND,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  StyleButton: {
    backgroundColor: COLOR.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 20,
    height: 40,
    width: 300,
    fontSize: 18,
    borderRadius: 5,
    fontFamily: 'iransans',
  },

  inputBox: {
    width: 300,
    height: 40,
    backgroundColor: COLOR.WHITE,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: COLOR.PRIMARY,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 8,
  },

  input: {
    flex: 1,
    width: 300,
    height: 40,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#002f6c',
    fontSize: 16,
    alignItems: 'center',
    fontFamily: 'iransanslight',
  },

  titleStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 0,
    fontFamily: 'iransansbold',
  },

  contentStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 8,
    marginBottom: 32,
    color: COLOR.TEXT_COLOR,
    fontFamily: 'iransanslight',
  },

  defaultLabelStyle: {
    color: COLOR.WHITE,
    fontSize: 16,
    fontFamily: 'iransanslight',
  },
});
