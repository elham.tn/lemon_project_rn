/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React from 'react';
import {
  Alert,
  StyleSheet,
  View,
  StatusBar,
  ScrollView,
  KeyboardAvoidingView,
  NetInfo,
  Platform,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';
import { Text } from 'native-base';
import COLOR from '../styles/Color';
import Icons from '../components/Icons';
import { BackHandler } from 'react-native';
import PersianNumber from '../components/PersianNumber.js';

console.disableYellowBox = true;

export default class SrcProfile extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      TxtUsername: '',
      correct: true,
      valueToshow: '',
      loading: true,
      dataSource: [],
      textToQR: 'Hello ....',

      verify_code: '',
      user_id: 0,
      user_token: '',
      user_active_complex: 0,
      user_mobile: '',
      user_phone: '',
      user_address: '',
      user_first_name: '',
      user_last_name: '',
      user_email: '',
      user_coin: 0,
      user_code_melli: '',
    };

    this._retrieveData();
    this.onBackPress = this.onBackPress.bind(this);
  }

  _retrieveData = async () => {
    try {
      const fname = await AsyncStorage.getItem('@KEY_USER_FIRST_NAME');
      const lname = await AsyncStorage.getItem('@KEY_USER_LAST_NAME');
      const active_complex = await AsyncStorage.getItem('@KEY_USER_ACTIVE_COMPLEX');
      if (fname !== null) {
        // Our data is fetched successfully

        this.setState({
          user_first_name: fname,
          user_last_name: lname,
          user_active_complex: Number(active_complex),
        });
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  CheckConnectivity = () => {
    // For Android devices
    if (Platform.OS === 'android') {
      NetInfo.isConnected.fetch().then(isConnected => {
        if (isConnected) {
          Alert.alert('You are online!');
        } else {
          Alert.alert('You are offline!');
        }
      });
    } else {
      // For iOS devices
      NetInfo.isConnected.addEventListener(
        'connectionChange',
        this.handleFirstConnectivityChange,
      );
    }
  };

  handleFirstConnectivityChange = isConnected => {
    NetInfo.isConnected.removeEventListener(
      'connectionChange',
      this.handleFirstConnectivityChange,
    );

    if (isConnected === false) {
      Alert.alert('You are offline!');
    } else {
      Alert.alert('You are online!');
    }
  };

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._retrieveData();
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    Alert.alert(
      ' خروج از برنامه ',
      ' آیا می خواهید از برنامه خارج شوید؟',
      [
        { text: 'بله', onPress: () => BackHandler.exitApp() },
        { text: 'خیر', onPress: () => console.log('NO Pressed') },
      ],
      { cancelable: false },
    );
    return true;
  };

  render() {
    return (
      <ScrollView style={{
        flex: 1,
        backgroundColor: COLOR.COLOR_BACKGROUND,
      }}>
        <KeyboardAvoidingView
          style={{
            flex: 1,
          }}
          behavior={Platform.OS === 'ios' ? 'padding' : null}>
          <View style={styles.containerStyle}>
            <StatusBar backgroundColor="white" barStyle="dark-content" />

            <Icons name={'profile'} />

            <View style={styles.styleInfoContainer}>
              <View style={styles.styleInfoSubContainer}>

                <Text style={styles.styleName}>{this.state.user_first_name + ' ' + this.state.user_last_name} </Text>

                <View style={styles.styleMainProviderNumContainer}>

                  <PersianNumber number={this.state.user_active_complex} size={24} />

                  <Text style={styles.styleActiveComplex}> تعداد مراکز فعال من :</Text>

                </View>

              </View>

            </View>

            <View style={styles.styleMainContainer} >

              <TouchableOpacity
                style={styles.styleSubContainer}
                onPress={() => {
                  this.props.navigation.navigate('ScrProviders');
                }}
              >
                <View style={styles.styleSubContainer}>
                  <Icons name={'list'} />
                  <Text
                    style={styles.styleSubContainerText}
                  > مراکز من </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.styleSubContainer}
                onPress={() => {
                    this.props.navigation.navigate('ScrSettings');
                }}
              >
                <View style={styles.styleSubContainer} >
                  <Icons name={'settings'} />
                  <Text style={styles.styleSubContainerText} > تنظیمات </Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.styleSubContainer}
                onPress={() => {
                  //  this.props.navigation.navigate('ScrProviders');
                }}
              >
                <View style={styles.styleSubContainer} >
                  <Icons name={'mb'} />
                  <Text style={styles.styleSubContainerText} > صندوق پیام (بزودی) </Text>
                </View>
              </TouchableOpacity>

            </View>

          </View>
        </KeyboardAvoidingView>
      </ScrollView >
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: COLOR.COLOR_BACKGROUND,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  styleMainContainer: {
    flex: 1,
    width: '90%',
    padding: 4,
    alignItems: 'center',
    flexDirection: 'row',
  },

  styleMainProviderNumContainer: {
    flex: 1,
    width: '50%',
    alignItems: 'center',
    flexDirection: 'row',
  },

  styleSubContainer: {
    flex: 1,
    margin: 4,
    borderRadius: 8,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
  },

  styleSubContainerText: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    alignItems: 'center',
    margin: 16,
    justifyContent: 'center',
    textAlign: 'center',
  },

  styleName: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    alignItems: 'center',
    marginTop: 16,
    justifyContent: 'center',
    fontSize: 30,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  styleActiveComplex: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    marginTop: 16,
    marginBottom: 16,
    fontSize: 18,
  },

  styleInfoContainer: {
    flex: 1,
    width: '90%',
    padding: 8,
    alignItems: 'center',
    flexDirection: 'row',
  },

  styleInfoSubContainer: {
    marginTop: 32,
    backgroundColor: COLOR.WHITE,
    flex: 1,
    width: '90%',
    padding: 8,
    alignItems: 'center',
    borderRadius: 8,
  },

});
