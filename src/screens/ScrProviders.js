/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  Alert,
  AsyncStorage,
  ActivityIndicator,
  Platform,
} from 'react-native';
import { Toolbar } from 'react-native-material-ui';
import { BackHandler } from 'react-native';

export default class ScrProviders extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      user_id: 0,
      user_token: '',
      providers: [],
      isFetching: false,
    };

    this.onBackPress = this.onBackPress.bind(this);
    this.onItemClick = this.onItemClick.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._retrieveData();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrProfile');
    return true;
  };

  _retrieveData = async () => {
    try {
      const user_id = await AsyncStorage.getItem('@KEY_USER_ID');
      const user_token = await AsyncStorage.getItem('@KEY_USER_TOKEN');
      if (user_id !== null) {
        // Our data is fetched successfully

        this.setState({
          user_id: user_id,
          user_token: user_token,
        });
        this.onFetchProfiders();
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  async onFetchProfiders() {
    try {
      this.setState({ loading: true });
      fetch('https://mylemon.ir/api/v1/complexes', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          user_id: this.state.user_id,
          token: this.state.user_token,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 1) {
            if (responseJson.code == 0) {
              console.log(responseJson.content.complexes[0].complex_name);

              this.setState({
                providers: [...this.state.providers, ...responseJson.content.complexes],
                loading: false,
                isFetching: false,
              });

            } else if (responseJson.code == 2002) {

              Alert.alert(
                ' خطا ',
                responseJson.message,
                [{ text: 'باشه', onPress: () => console.log('NO Pressed') }],
                { cancelable: false },
              );
            }
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ error, loading: false, isFetching: false });
        });
    } catch (errors) {
      // eslint-disable-next-line no-alert
      alert(errors);
    }
  }

  FlatListItemSeparator = () => {
    return (
      //Item Separator
      <View style={{ height: 0.5, width: '100%', backgroundColor: '#FAFAFA' }} />
    );
  };

  onItemClick(item) {
    this._storeData(item);
  }

  _storeData = async (item) => {
    try {
      await AsyncStorage.setItem('@KEY_PROVIDER_ID', item.complex_id + '');
      await AsyncStorage.setItem('@KEY_PROVIDER_NAME', item.complex_name);
      await AsyncStorage.setItem('@KEY_MANAGER_NAME', item.manager_name);
      await AsyncStorage.setItem('@KEY_MANAGER_EMAIL', item.manager_email);
      await AsyncStorage.setItem('@KEY_MANAGER_ADDRESS', item.manager_address);
      await AsyncStorage.setItem('@KEY_MANAGER_PHONE', item.manager_phone);
      await AsyncStorage.setItem('@KEY_PROVIDER_AVATAR', item.avatar);
      await AsyncStorage.setItem('@KEY_USER_NEXT_VISIT', item.next_visit);
      await AsyncStorage.setItem('@KEY_PROVIDER_RATE', item.rate + '');

      this.props.navigation.navigate('ScrProvidersProfile');
    } catch (error) {
      // Error retrieving data
    }
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.onItemClick(item)}>
        <View style={styles.rowStyle}>
          <Image
            source={{ uri: item.avatar }}
            style={styles.imageStyle}
          />
          <View style={styles.textRowStyle}>
            <Text style={styles.headerStyle}>{item.complex_name}</Text>
            <Text style={styles.contentStyle}>{item.manager_name}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  onRefresh() {
    this.setState({ isFetching: true, providers: [] },

      function () { this.onFetchProfiders() });
  }

  render() {
    return (
      <View style={styles.container}>

        <Toolbar
          style={{
            container: { backgroundColor: 'white' },
            titleText: { color: '#000', fontFamily: 'iransans', alignSelf: 'flex-start' },
            leftElement: { color: '#000' },
          }}
          leftElement="arrow-back"
          centerElement="مراکز من"
          onLeftElementPress={(label) => { console.log(label) }}
        />

        <View style={styles.MainContainer}>
          <StatusBar
            animated={true}
            barStyle="dark-content"
            backgroundColor="white"
          />

          {
            (this.state.loading)
              ?
              (<ActivityIndicator size="large" />)
              :
              (
                <FlatList
                  extraData={this.state}
                  padding={10}
                  data={this.state.providers}
                  keyExtractor={(item, index) => index}
                  renderItem={this.renderItem}
                  removeClippedSubviews={true}
                  initialNumToRender={2}
                  minToRenderPerBatch={10}
                  maxToRenderPerBatch={100}
                  windowSize={7}
                  onRefresh={() => this.onRefresh()}
                  refreshing={this.state.isFetching}
                />
              )
          }
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#FFFFFF',
  },

  MainContainer: {
    justifyContent: 'center',
    flex: 1,
    marginTop: 8,
    paddingTop: (Platform.OS === 'ios') ? 20 : 0,
  },

  rowStyle: {
    marginTop: 4,
    padding: 4,
    flex: 1,
    flexDirection: 'row-reverse',
  },

  textRowStyle: {
    justifyContent: 'space-around',
    padding: 8,
  },

  imageStyle: {
    width: 70,
    height: 70,
    margin: 4,
  },

  headerStyle: {
    color: '#121212',
    fontSize: 18,
    flex: 1,
    fontWeight: 'bold',
    fontFamily: 'roboto-regular',
  },

  contentStyle: {
    color: '#121212',
    flex: 1,
    fontSize: 14,
    fontFamily: 'roboto-700',
  },

});
