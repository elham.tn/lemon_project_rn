/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  StatusBar,
  AsyncStorage,
  TouchableOpacity,
  Text,
  Image,
  ScrollView,
  Alert,
  Clipboard,
  Share,
  Vibration,
  ActivityIndicator,
} from 'react-native';
import { BackHandler } from 'react-native';
import COLOR from '../styles/Color';
import Icons from '../components/Icons';
import PersianNumber from '../components/PersianNumber.js';
import moment from 'moment-jalaali';
import fa from 'moment/src/locale/fa';

export default class ScrProvidersProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      u_id: 0,
      u_token: '',
      m_name: '',
      c_name: '',
      c_rate: 0,
      c_avatar: '',
      u_referral_code: '',
      u_ref_address: '',
      u_next_visit: '',
      u_visit: 0,
      u_credit: 0,
      day: 0,
      month: 0,
      year: 0,
      day_betweens: 0,
    };

    this.onBackPress = this.onBackPress.bind(this);
    this.onShare = this.onShare.bind(this);
    this.onFetchProfileCredits = this.onFetchProfileCredits.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
    this._retrieveData();

    moment.locale('fa', fa);
    moment.loadPersian();
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrProfile');
    return true;
  };

  setCurrentPersianDate = () => {
    this.setState({
      year: new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day: new Date().getDate(),
    });
    Alert.alert(new Date().getFullYear() + '-' + new Date().getMonth() + '-' + new Date().getDate());
  }

  _retrieveData = async () => {
    try {
      const user_id = await AsyncStorage.getItem('@KEY_USER_ID');
      const user_token = await AsyncStorage.getItem('@KEY_USER_TOKEN');
      const c_id = await AsyncStorage.getItem('@KEY_PROVIDER_ID');
      const c_name = await AsyncStorage.getItem('@KEY_PROVIDER_NAME');
      const m_name = await AsyncStorage.getItem('@KEY_MANAGER_NAME');
      const c_avatar = await AsyncStorage.getItem('@KEY_PROVIDER_AVATAR');
      const u_next_visit = await AsyncStorage.getItem('@KEY_USER_NEXT_VISIT');
      const c_rate = await AsyncStorage.getItem('@KEY_PROVIDER_RATE');

      if (user_id !== null) {
        this.setState({
          u_id: user_id,
          u_token: user_token,
          c_id: c_id,
          c_name: c_name,
          m_name: m_name,
          c_avatar: c_avatar,
          u_next_visit: moment(u_next_visit, 'YYYY/M/D HH:mm').format('jYYYY/jM/jD HH:mm'),
          c_rate: Number(c_rate),
        });
        this.onFetchProfileCredits();
      }
    } catch (error) {
      // Error retrieving data
    }
  }

  async onFetchProfileCredits() {
    try {
      fetch('https://mylemon.ir/api/v1/complex/credit', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: this.state.u_token,
          user_id: this.state.u_id,
          complex_id: this.state.c_id,
        }),
      })
        .then(response => response.json())
        .then(responseJson => {
          //  console.log(responseJson);
          if (responseJson.status == 1) {
            if (responseJson.code == 0) {

              this.setState({
                u_credit: responseJson.content.complex.credit,
                u_visit: responseJson.content.complex.visit,
                u_referral_code: responseJson.content.complex.ref_code,
                u_ref_address: responseJson.content.complex.ref_address,
                isFetching: false,
                loading: false,
              });

            } else if (responseJson.code == 2002) {

              Alert.alert(
                ' خطا ',
                responseJson.message,
                [{ text: 'باشه', onPress: () => console.log('NO Pressed') }],
                { cancelable: false },
              );
            }
          }
        })
        .catch(error => {
          console.error(error);
          this.setState({ error, loading: false });
        });
    } catch (errors) {
      //  alert(errors);
    }
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.u_ref_address,
      });
      if (result.action == Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      // eslint-disable-next-line no-alert
      alert(error.message);
    }
  };

  render() {
    return (
      (this.state.loading)
        ?
        (<ActivityIndicator size="large" />)
        :
        (
          <ScrollView style={{
            flex: 1,
            backgroundColor: COLOR.COLOR_BACKGROUND,
          }}>

            <View style={styles.container}>

              <StatusBar
                animated={true}
                barStyle="dark-content"
                backgroundColor="white"
              />
              <Icons name={'back'} />

              <View style={styles.providerInfoContainer}>

                <Image
                  source={{ uri: this.state.c_avatar }}
                  style={styles.imageStyle} />

                <View style={styles.textRowStyle}>
                  <Text style={styles.headerStyle}>{this.state.c_name}</Text>
                  <Text style={styles.contentStyle}>{this.state.m_name}</Text>
                  <View style={{ position: 'absolute', padding: 4, flexDirection: 'row', bottom: 0, right: 0 }}>
                    <PersianNumber number={this.state.c_rate} size={18} />
                    <Icons name={'star'} />
                  </View>

                </View>

                <TouchableOpacity
                  style={{ alignSelf: 'center', marginLeft: 8 }}
                >
                  <View style={styles.profileButtonStyle}>
                    <Icons name={'back-small'} />
                    <Text style={styles.contentStyle}>پروفایل</Text>
                  </View>
                </TouchableOpacity>

              </View>

              <View style={styles.cardContainer}>

                <View style={styles.cardColumnStyle}>
                  <Icons name={'wallet'} />
                  <TouchableOpacity
                    style={{ alignSelf: 'flex-start', position: 'absolute', bottom: 0 }}
                    onPress={() => this.props.navigation.navigate('ScrHistory')}
                  >
                    <View style={styles.pointsButtonStyle}>
                      <Icons name={'back-small'} />
                      <Text style={styles.pointStyle}>تاریخچه امتیازات</Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <View style={styles.cardColumnStyle}>
                  <Text style={styles.pointStyle}>اعتبار شما</Text>
                  <View style={{ alignItems: 'center', padding: 4 }}>
                    <PersianNumber
                      number={this.state.u_credit}
                      size={100}
                      color={COLOR.COLOR_BACKGROUND}
                    />
                  </View>
                </View>

              </View>

              <View style={styles.styleRefContainer} >

                <View style={styles.styleRefTopContainer}>

                  <View style={styles.styleRefTopSubContainer}>
                    <Text style={styles.refTextStyle}>{this.state.u_referral_code}</Text>
                    <Text style={styles.refrightTextStyle}>کد معرف</Text>
                  </View>

                  <View style={{ backgroundColor: '#dadada', height: 1, marginTop: 8 }} />

                </View>

                <View style={styles.styleRefBtnContainer}>

                  <TouchableOpacity
                    style={styles.styleRefSubContainer}
                    onPress={() => {
                      // this.props.navigation.navigate('ScrProviders');
                    }}
                  >
                    <View style={styles.styleRefSubContainer}>
                      <Icons name={'info'} />
                      <Text
                        style={styles.styleSubContainerText}
                      >  کسب اعتبار </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.styleRefSubContainer}
                    onPress={() => {
                      Clipboard.setString(this.state.u_ref_address);
                      Vibration.vibrate();
                    }}
                  >
                    <View style={styles.styleRefSubContainer} >
                      <Icons name={'copy'} />
                      <Text style={styles.styleSubContainerText} > کپی </Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.styleRefSubContainer}
                    onPress={() => {
                      this.onShare();
                    }}
                  >
                    <View style={styles.styleRefSubContainer} >
                      <Icons name={'share'} />
                      <Text style={styles.styleSubContainerText} > اشتراک گزاری </Text>
                    </View>
                  </TouchableOpacity>

                </View>

              </View>

              <View style={styles.styleLastContainer}>

                <View style={styles.styleSubContainer}>

                  <View style={styles.styleSubSubContainer}>

                    <Text style={styles.styleSubContainerTextBold} >
                      {moment().format('dddd')}</Text>

                    <PersianNumber number={moment().format('jYYYY/jM/jD')} size={18} b_margin={16}/>

                  </View>

                  <Text style={styles.styleSubContainerText} >تقویم</Text>

                </View>

                <View style={styles.styleSubContainer}>

                  <View style={styles.styleSubSubContainer}>

                    <Icons name={'calendar'} />

                    <PersianNumber number={this.state.u_next_visit} size={18} t_margin={16} />

                  </View>

                  <Text style={styles.styleSubContainerText} >نوبت بعدی</Text>

                </View>

                <View style={styles.styleSubContainer}>

                  <View style={styles.styleSubSubContainer}>

                    <PersianNumber
                      number={this.state.u_visit}
                      size={50}
                      color={COLOR.TEXT_COLOR}
                      t_margin={25}
                    />
                  </View>

                  <Text style={styles.styleSubContainerText} >تعداد حضور</Text>

                </View>

              </View>

            </View>

          </ScrollView >
        )
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLOR.COLOR_BACKGROUND,
  },

  providerInfoContainer: {
    padding: 4,
    margin: 8,
    direction: 'rtl',
    flexDirection: 'row-reverse',
  },

  cardColumnStyle: {
    padding: 4,
    flex: 1,
    flexDirection: 'column',
  },

  cardContainer: {
    padding: 4,
    marginLeft: 100,
    direction: 'rtl',
    height: 180,
    backgroundColor: COLOR.COLOR_GREEN,
    flexDirection: 'row',
    borderBottomLeftRadius: 8,
    borderTopLeftRadius: 8,
  },

  textRowStyle: {
    padding: 8,
    flex: 2,
  },

  imageStyle: {
    width: 70,
    height: 70,
    margin: 4,
  },

  headerStyle: {
    color: '#121212',
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'roboto-regular',
  },

  contentStyle: {
    color: '#121212',
    fontSize: 14,
    fontFamily: 'roboto-700',
  },

  pointStyle: {
    color: COLOR.COLOR_BACKGROUND,
    fontSize: 14,
    fontFamily: 'roboto-700',
    padding: 4,
  },

  profileButtonStyle: {
    width: 80,
    alignSelf: 'center',
    alignItems: 'center',
    borderColor: COLOR.PRIMARY,
    borderRadius: 5,
    flexDirection: 'row',
    padding: 4,
    direction: 'ltr',
    borderWidth: 1,
  },

  pointsButtonStyle: {
    width: 100,
    alignSelf: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 4,
    direction: 'ltr',
  },

  styleRefContainer: {
    flex: 1,
    padding: 2,
    width: '90%',
    flexDirection: 'column',
    margin: 8,
    alignSelf: 'center',
    backgroundColor: COLOR.WHITE,
  },

  styleLastContainer: {
    flex: 1,
    padding: 2,
    width: '90%',
    flexDirection: 'row',
    alignSelf: 'center',
  },

  styleRefTopContainer: {
    flex: 1,
    flexDirection: 'column',
    margin: 8,
  },

  styleRefSubContainer: {
    flex: 1,
    alignItems: 'center',
  },

  styleRefTopSubContainer: {
    flex: 1,
    flexDirection: 'row',
    padding: 4,
    alignSelf: 'stretch',
  },

  styleRefBtnContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
  },

  refTextStyle: {
    color: COLOR.TEXT_COLOR,
    fontSize: 14,
    fontFamily: 'roboto-700',
    padding: 4,
    marginLeft: 8,
  },

  refrightTextStyle: {
    color: COLOR.TEXT_COLOR,
    fontSize: 14,
    fontFamily: 'roboto-700',
    padding: 4,
    position: 'absolute',
    right: 16,
    top: 8,
  },

  styleSubContainerText: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    alignItems: 'center',
    margin: 10,
    justifyContent: 'center',
    textAlign: 'center',
  },

  styleSubContainerTextBold: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    alignItems: 'center',
    marginTop: 20,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 20,
  },

  styleSubContainerTextExtraBold: {
    flex: 1,
    color: COLOR.TEXT_COLOR,
    justifyContent: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 40,
    marginTop: 30,
  },

  styleSubContainer: {
    flex: 1,
    alignItems: 'center',
  },

  styleSubSubContainer: {
    flex: 1,
    height: 100,
    width: '90%',
    borderRadius: 8,
    backgroundColor: COLOR.WHITE,
    alignItems: 'center',
  },

});
