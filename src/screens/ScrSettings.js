/* eslint-disable prettier/prettier */
import React from 'react';
import {
  Alert,
  SafeAreaView,
  StyleSheet,
  View,
  StatusBar,
  Image,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  Text,
} from 'react-native';
import Icons from '../components/Icons';
import { BackHandler } from 'react-native';
import { Toolbar } from 'react-native-material-ui';
import COLOR from '../styles/Color';

console.disableYellowBox = true;

export default class ScrSettings extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
    this.onBackPress = this.onBackPress.bind(this);
    this.onLogOutPress = this.onLogOutPress.bind(this);
  }

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {
    this.props.navigation.navigate('ScrProfile');
    return true;
  };

  onLogOutPress = () => {
    Alert.alert(
      ' خروج از برنامه ',
      ' آیا می خواهید از حساب کاربری خود خارج شوید؟',
      [
        {
          text: 'بله',
          onPress: () => {
            this._storeData();
            this.props.navigation.navigate('ScrLogin');
          },
        },
        { text: 'خیر', onPress: () => console.log('NO Pressed') },
      ],
      { cancelable: false },
    );
  };

  _storeData = async () => {
    try {
      await AsyncStorage.setItem('@KEY_SIGN_IN', 'true');
      await AsyncStorage.setItem('@KEY_FIRST_TIME', 'false');
    } catch (error) {
      // Error retrieving data
    }
  };

  render() {
    return (
      <SafeAreaView>
        <Toolbar
          style={{
            container: { backgroundColor: 'white' },
            titleText: { color: '#000', fontFamily: 'iransans', alignSelf: 'flex-start' },
            leftElement: { color: '#000' },
          }}
          leftElement="arrow-back"
          centerElement="تنظیمات"
          onLeftElementPress={(label) => { console.log(label); }}
        />

        <ScrollView>
          <View style={styles.container}>
            <StatusBar
              animated={true}
              barStyle="dark-content"
              backgroundColor="white"
            />

            <View style={styles.rowContainer}>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate('ScrEditProfile');
                }}>
                <Image
                  source={require('../images/portfolio.png')}
                  style={styles.imgInRowStyle}
                />

                <Text style={styles.txtInRowStyle}> ویرایش پروفایل </Text>
              </TouchableOpacity>

            </View>

            <View style={styles.rowContainer}>

              <TouchableOpacity
                onPress={() => {
                  this.onLogOutPress();
                }}>
                <Image
                  source={require('../images/exit.png')}
                  style={styles.imgInRowStyle}
                />

                <Text style={styles.txtInRowStyle}> خروج از حساب کاربری </Text>
              </TouchableOpacity>

            </View>

          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:COLOR.COLOR_BACKGROUND,
  },

  rowContainer: {
    flexDirection: 'row-reverse',
    width: '100%',
    padding: 16,
  },

  imgInRowStyle: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    width: 20,
    height: 20,
    right: 0,
    position: 'absolute',
  },

  txtInRowStyle: {
    right: 28,
    fontFamily: 'iransans',
    fontSize: 16,
  },
});
