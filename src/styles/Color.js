/* eslint-disable prettier/prettier */
'use strict';

const COLOR = {
  PRIMARY: '#90E36A',
  PRIMARY_DARK: '#392b5b',
  PRIMARY_LIGHT: '#85BDB9',
  PRIMARY_SECONDARY:'#FFBB00',
  COLOR_GREEN:'#00AA00',
  ACCENT: '#8b61ff',
  TEXT_COLOR: '#2e2e1f',
  COLOR_BACKGROUND: '#FAFAFA',
  COLOR_INACTIVE_NAV: '#BBB9B9',
  COLOR_EDT_HINT: '#535A70',

  WHITE: '#fff',
  BLACK: '#000',
  RED: '#f54b5e',
  GREY: '#8f8f8f',
};

export default COLOR;
